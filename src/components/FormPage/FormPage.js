import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {CATEGORIES} from "../../constans";
import axios from "../../axios-pages";

class FormPage extends Component {
    state = {
        title: '',
        category: Object.keys(CATEGORIES)[0],
        content: '',
        currentCategory: ''
      };

  valueChanged = event =>{
    const {value} = event.target;

    axios.get(`pages/${value}.json`).then(response=>{
      this.setState({title: response.data.title ,content: response.data.content ,currentCategory: value,})
    })
  };



  changeHandler = (event) => {
    this.setState({[event.target.name] : event.target.value})
  };

  editPage = (event) => {
    event.preventDefault();
    const page = {
      title: this.state.title,
      content: this.state.content
    };

    axios.put(`pages/${this.state.currentCategory}.json`, page).then(() => {
      this.props.history.push(`/pages/${this.state.currentCategory}`);
    });
  };

  render() {
    return (
      <Form className="PagesForm" onSubmit={(e) => this.editPage(e)}>
        <FormGroup row>
          <Label for="category" sm={2}>Pages</Label>
          <Col sm={10}>
            <Input type="select" name="category" id="category"
                    onChange={this.valueChanged}
            >
              <option selected disabled value="">choice your category</option>
              {Object.keys(CATEGORIES).map(categoryID => (
                <option key={categoryID} value={categoryID}>{CATEGORIES[categoryID]}</option>
              ))}
            </Input>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for="title" sm={2}>Title</Label>
          <Col sm={10}>
            <Input type="text" name="title" id="title" placeholder="Title"
                   value={this.state.title} onChange={(e) => this.changeHandler(e)}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for="content" sm={2}>Content</Label>
          <Col sm={10}>
            <Input type="textarea" name="content" placeholder="Content"
                   value={this.state.content} onChange={(e) => this.changeHandler(e)}
            />
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col sm={{size: 10, offset: 2}}>
            <Button type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default FormPage;