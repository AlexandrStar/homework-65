import React from 'react';

import NavigationItem from "./NavigationItem/NavigationItem";

import './NavigationItems.css';
import {CATEGORIES} from "../../../constans";

const NavigationItems = () => (
  <ul className="NavigationItems">
    <NavigationItem to="/" exact>Home</NavigationItem>
    {Object.keys(CATEGORIES).map(categoryId => (
        <NavigationItem
          key={categoryId}
          to={"/pages/" + categoryId}
          exact
        >
          {CATEGORIES[categoryId]}
        </NavigationItem>
    ))}
    <NavigationItem to="/pages/edit">Admin</NavigationItem>
    {/*<NavigationItem to="/pages/contacts">Contacts</NavigationItem>*/}
  </ul>
);


export default NavigationItems;