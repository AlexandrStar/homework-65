import React from 'react';

import './Logo.css';
import burgerLogo from '../../assets/images/retro_sega_logo.gif';

const Logo = () => (
  <div className="Logo">
    <img src={burgerLogo} alt="MyBurger" />
  </div>
);


export default Logo;