import React, {Component} from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router";

import Pages from "./containers/Pages";

import './App.css';
import {Container} from "reactstrap";
import FormPage from "./components/FormPage/FormPage";

class App extends Component {
  render() {
    return (
      <Container>
        <Layout>
          <Switch>
            <Route path="/" exact render={() => <h1>Home</h1>} />
            <Route path="/pages/edit" component={FormPage} />
            <Route path="/pages/:categoryId" component={Pages} />
            <Route render={() => <h1>No found!</h1>} />
          </Switch>
        </Layout>
      </Container>
    );
  }
}

export default App;
