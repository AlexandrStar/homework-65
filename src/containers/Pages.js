import React, {Component} from 'react';
import axios from '../axios-pages';
import Spinner from "../components/UI/Spinner/Spinner";
import {Card, CardText, CardTitle, Col} from "reactstrap";

class Pages extends Component {
  state ={
    pages: null,
    loading: true
  };

  loadData() {
    let url = `pages/${this.props.match.params.categoryId}.json`;
    axios.get(url).then(response => {
      if (!response.data) return;
      this.setState({pages: response.data, loading: false});
    })
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
      this.loadData();
    }
  }

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }

    return (
        <Col sm={9}>
          <Card style={{marginBottom: "15px"}} body>
            <CardTitle>--{this.state.pages.title}</CardTitle>
            <CardText>{this.state.pages.content}</CardText>
          </Card>
        </Col>
    );
  }
}

export default Pages;