import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://hw65-alexandrcher.firebaseio.com/'
});

export default instance;