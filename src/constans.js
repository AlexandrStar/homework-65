export const CATEGORIES = {
  'about': 'About',
  'contacts': 'Contacts',
  'divisions': 'Divisions',
  'music': 'Music',
  'film': 'Film',
  'info': 'Info'
};